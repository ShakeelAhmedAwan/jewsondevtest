# README #

### What is this repository for? ###

* Source code for Jewson dev test

### How do I get set up? ###

* Download the JewsonDevTest compressed file (there should be a Downloads link in the navigation on the left)
* Uncompress if and open JewsonDevTest.sln file in Visual Studio
* Rebuild complete solution (It'll download all nuget packages required)
* Run JewsonDevTestApp
* Application uses APIEndPoint config setting to communicate to API so make sure its pointing to right url